json.extract! votescomment, :id, :comment_id, :user_id, :created_at, :updated_at
json.url votescomment_url(votescomment, format: :json)

class ContributionsController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :vote, :unvote, :update, :destroy]
  
  # GET /contributions
  # GET /contributions.json
  def index
    @contributions = Contribution.all.order(points: :desc)
    respond_to do |format|
      format.json { render json: @contributions, status: :ok }
    end
  end
  
  def newest
    @contributions = Contribution.all.order(created_at: :desc)
    respond_to do |format|
      format.json { render json: @contributions, status: :ok }
    end
  end
  
  def contributionsUser
    session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
    @contributions = Contribution.all.where(userID: params[:idUser])
    respond_to do |format|
      format.json { render json: @contributions, status: :ok }
    end
  end
  
  def upvotedContributions
    
    respond_to do |format|
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else
        session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
        @contributions_aux = Contribution.all
        @contributions = []
        
        @contributions_aux.each do |contribution|
          if (contribution.usersVoted.include?(','+ session[:user]["id"].to_s + ',') )
            @contributions << contribution
          end
        end
        format.json { render json: @contributions, status: :ok}
      end
    end
  end
  
  def ask
    @contributions = Contribution.where(url: "")
    respond_to do |format|
      format.json { render json: @contributions, status: :ok }
    end
  end
  
  def vote
    respond_to do |format|
      
      session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      elsif (@contribution.usersVoted.include?(","+session[:user]["id"].to_s))
        format.json { render json: {status: 400, error: "Bad Request", message: "You have already voted it"}, status: 400 }
      else
        @contribution.usersVoted << session[:user]["id"].to_s+","
        @contribution.points = @contribution.points + 1
        @contribution.save
        format.html { redirect_to request.referrer }
        format.json { render json: {message: "Voted successfully"}, status: :ok}
      end
    end
  end
  
  def unvote
    respond_to do |format|
    
      session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      elsif (@contribution.usersVoted.exclude?(","+session[:user]["id"].to_s))
        format.json { render json: {status: 400, error: "Bad Request", message: "You have already unvoted it"}, status: 400 }
      else
        @contribution.usersVoted.slice!(session[:user]["id"].to_s+",")
        @contribution.points = @contribution.points - 1
        @contribution.save
        format.html { redirect_to request.referrer }
        format.json { render json: {message: "Unvoted successfully"}, status: :ok}
      end
    end
  end
  
  # GET /contributions/1
  # GET /contributions/1.json
  def show
    @comments = Comment.all.where(contributions_id: params[:id])
    respond_to do |format|
      format.json { render json: @comments, status: :ok }
    end
  end

  # GET /contributions/new
  def new
    @contribution = Contribution.new
    
  end

  # GET /contributions/1/edit
  def edit
  end

  # POST /contributions
  # POST /contributions.json
  def create
    @contribution = Contribution.new(contribution_params)
    respond_to do |format|
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else
        session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
        @contribution.user = session[:user]["name"]
        @contribution.userID = session[:user]["id"]
        
        
            
        if @contribution.url.empty? || !Contribution.exists?(url: @contribution.url)
          
          if @contribution.title.empty? || (@contribution.url.empty? && @contribution.text.empty?)
            format.html { redirect_to new_contribution_path, alert: "FIELDS MUST BE FILLED: title, url or text" }
            format.json { render json: {status: 400, error: "Bad Request", message: "FIELDS MUST BE FILLED: title, url or text"}, status: 400 }
          elsif !@contribution.url.empty? && !@contribution.url.include?("https")
            format.html { redirect_to new_contribution_path, alert: "Wrong format of URL!" }
            format.json { render json: {status: 400, error: "Bad Request", message: "Wrong format of URL!"}, status: 400 }
          else 
            if @contribution.save
              
              if !@contribution.url.empty? && !@contribution.text.empty?
                 @comment = Comment.new(text: @contribution.text, author_id: @contribution.userID, author: @contribution.user, contributions_id: @contribution.id)
                 @comment.save
              end
              format.html { redirect_to newest_path}
              format.json { render json: {new_contribution: @contribution, api_key: request.headers["X-API-KEY"]}, status: 201}
                
            else
              format.html { render :new }
              format.json { render json: @contribution.errors, status: :unprocessable_entity }
            end
          end
        else
          format.html { redirect_to show_contributions_path(Contribution.all.find_by(url: @contribution.url)) }
          format.json { render json: {redirectedTo: Contribution.all.find_by(url: @contribution.url), message: "URL repeated"}, status: :ok}
        end
      end
    end
  end

  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  def update
    respond_to do |format|
      if @contribution.update(contribution_params)
        format.html { redirect_to @contribution, notice: 'Contribution was successfully updated.' }
        format.json { render :show, status: :ok, location: @contribution }
      else
        format.html { render :edit }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    @contribution.destroy
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  
  def showNotLogMessage
    respond_to do |format|
      if (session[:user].nil?)
       format.html {redirect_to root_path, notice: "You have to be logged in to submit."}
      else 
       format.html {redirect_to root_path}
      end
    end
  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def contribution_params
      params.require(:contribution).permit(:title, :url, :points, :user, :text)
    end
end

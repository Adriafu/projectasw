class RepliesController < ApplicationController
  before_action :set_reply, only: [:show, :edit, :update, :destroy]

  # GET /replies
  # GET /replies.json
  def index
    @replies = Reply.all
  end

  # GET /replies/1
  # GET /replies/1.json
  def show
  end

  # GET /replies/new
  def new
    @reply = Reply.new
  end

  # GET /replies/1/edit
  def edit
  end

  # POST /replies
  # POST /replies.json
  def create
    @reply = Reply.new(reply_params)
    
    respond_to do |format|
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else
        session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
        
        if (@reply.text.empty?)
          format.json { render json: {status: 400, error: "Bad Request", message: "FIELDS MUST BE FILLED: title, url or text"}, status: 400 }
        else
          if @reply.save
            @comment = Comment.new(text: @reply.text, author_id: session[:user]["id"], author: session[:user]["name"], comments_id: @reply.comm_id, contributions_id: Comment.all.find_by(id: @reply.comm_id).contributions_id)
            @comment.save
            @reply.update(repl_id: @comment.id)
            
            format.html { redirect_to show_contributions_path(@comment.contributions_id)}
            format.json { render json: {new_reply: @reply, api_key: request.headers["X-API-KEY"]}, status: 201}
              
          else
            format.html { render :new }
            format.json { render json: @reply.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # PATCH/PUT /replies/1
  # PATCH/PUT /replies/1.json
  def update
    respond_to do |format|
      if @reply.update(reply_params)
        format.html { redirect_to @reply, notice: 'Reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @reply }
      else
        format.html { render :edit }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /replies/1
  # DELETE /replies/1.json
  def destroy
    @reply.destroy
    respond_to do |format|
      format.html { redirect_to replies_url, notice: 'Reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reply
      @reply = Reply.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def reply_params
      params.require(:reply).permit(:comm_id, :repl_id, :text, :root_id)
    end
end

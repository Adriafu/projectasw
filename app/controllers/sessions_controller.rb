class SessionsController < ApplicationController
  def omniauth
    user = User.from_omniauth(request.env['omniauth.auth'])
    if user.valid?
      session[:user] = user
      redirect_to contributions_path
    else
      redirect_to '/login'
    end
  end
  
  def destroy
    session.delete(:user)
    redirect_to root_path
  end 
end
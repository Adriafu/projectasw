class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy, :vote, :unvote]

  # GET /comments
  # GET /comments.json
  def index
    @comments = Comment.all
    respond_to do |format|
      format.json { render json: @comments, status: :ok }
    end
  end
  
  def commentsUser
    @comments = Comment.all.where(author_id: params[:idUser])
    respond_to do |format|
      format.json { render json: @comments, status: :ok }
    end
  end
  
  def upvotedComments
    
    respond_to do |format|
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else
        session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
        @votescomments = Votescomment.all.where(user_id: session[:user]["id"])
        
        @comments = []
        @votescomments.each do |votescomment|
            @comments << Comment.all.find_by(id: votescomment.comment_id)
        end
        format.json { render json: @comments, status: :ok}
      end
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
    respond_to do |format|
      format.json { render json: @comment, status: :ok }
    end
  end
  
  def vote
    respond_to do |format|
      
      session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      elsif !Votescomment.all.find_by(comment_id: @comment.id, user_id: session[:user]["id"]).nil?
        format.json { render json: {status: 400, error: "Bad Request", message: "You have already voted it"}, status: 400 }
      else
        @votescomment = Votescomment.new(comment_id: @comment.id, user_id: session[:user]["id"])
        @votescomment.save
        format.html { redirect_to request.referrer}
        format.json { render json: {message: "Voted successfully"}, status: :ok}
      end
    end
  end
  
  def unvote
    respond_to do |format|
      
      session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
      
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      elsif Votescomment.all.find_by(comment_id: @comment.id, user_id: session[:user]["id"]).nil?
        format.json { render json: {status: 400, error: "Bad Request", message: "You must vote it before"}, status: 400 }
      else
        @votescomment = Votescomment.all.find_by(comment_id: @comment.id, user_id: session[:user]["id"])
        @votescomment.destroy
        format.html { redirect_to request.referrer}
        format.json { render json: {message: "Unvoted successfully"}, status: :ok}
      end
    end
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @comment = Comment.new(comment_params)
    
    respond_to do |format|
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {status: 401, error: "Unauthorized", message: "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (User.all.find_by(token: request.headers["X-API-KEY"]).nil?)
        format.json { render json: {status: 403, error: "Forbidden", message: "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else
        session[:user] = User.all.find_by(token: request.headers["X-API-KEY"])
        
        if (@comment.text.empty?)
          format.json { render json: {status: 400, error: "Bad Request", message: "FIELDS MUST BE FILLED: title, url or text"}, status: 400 }
        else
          @comment.author_id = session[:user]["id"]
          @comment.author = session[:user]["name"]
          if @comment.save
            format.html { redirect_to show_contributions_path(@comment.contributions_id) }
            format.json { render json: {new_comment: @comment, api_key: request.headers["X-API-KEY"]}, status: 201 }
          else
            format.html { render :new }
            format.json { render json: @comment.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment.destroy
    respond_to do |format|
      format.html { redirect_to comments_url, notice: 'Comment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def comment_params
      params.require(:comment).permit(:text, :contributions_id)
    end
end

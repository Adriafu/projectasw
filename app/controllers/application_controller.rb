class ApplicationController < ActionController::Base
  protect_from_forgery unless: -> { request.format.json? }
  def hello
    render html: "<h1>It works WASLAB05!</h1>".html_safe
  end
  def newest
    render html: newest.html.erb
  end
end

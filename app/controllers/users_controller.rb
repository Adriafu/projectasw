class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    respond_to do |format|
      format.json{ render json: {id: @user.id, name: @user.name, created_at: @user.created_at, about: @user.about}, status: :ok }
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    
    respond_to do |format|
      if (request.headers["X-API-KEY"].nil?)
        format.json { render json: {:status => 401, :error => "Unauthorized", :message => "You provided no api key (X-API-KEY Header)"}, status: 401 }
      elsif (@user.token != request.headers["X-API-KEY"])
        format.json { render json: {:status => 403, :error => "Forbidden", :message => "Your api key (X-API-KEY Header) is not valid"}, status: 403 }
      else    
        if @user.update(user_params)
          format.html { redirect_to :root }
          format.json { render json: {id: @user.id, name: @user.name, email: @user.email, created_at: @user.created_at, token: @user.token, about: @user.about, maxvisit: @user.maxvisit, minaway: @user.minaway, delay: @user.delay}, status: :ok, location: @user }
        else
          format.html { render :edit }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:name, :email, :about, :maxvisit, :minaway, :delay)
    end
end

class VotescommentsController < ApplicationController
  before_action :set_votescomment, only: [:show, :edit, :update, :destroy]

  # GET /votescomments
  # GET /votescomments.json
  def index
    @votescomments = Votescomment.all
  end

  # GET /votescomments/1
  # GET /votescomments/1.json
  def show
  end

  # GET /votescomments/new
  def new
    @votescomment = Votescomment.new
  end

  # GET /votescomments/1/edit
  def edit
  end

  # POST /votescomments
  # POST /votescomments.json
  def create
    @votescomment = Votescomment.new(votescomment_params)

    respond_to do |format|
      if @votescomment.save
        format.html { redirect_to @votescomment, notice: 'Votescomment was successfully created.' }
        format.json { render :show, status: :created, location: @votescomment }
      else
        format.html { render :new }
        format.json { render json: @votescomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /votescomments/1
  # PATCH/PUT /votescomments/1.json
  def update
    respond_to do |format|
      if @votescomment.update(votescomment_params)
        format.html { redirect_to @votescomment, notice: 'Votescomment was successfully updated.' }
        format.json { render :show, status: :ok, location: @votescomment }
      else
        format.html { render :edit }
        format.json { render json: @votescomment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /votescomments/1
  # DELETE /votescomments/1.json
  def destroy
    @votescomment.destroy
    respond_to do |format|
      format.html { redirect_to votescomments_url, notice: 'Votescomment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_votescomment
      @votescomment = Votescomment.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def votescomment_params
      params.require(:votescomment).permit(:comment_id, :user_id)
    end
end

require "test_helper"

class VotescommentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @votescomment = votescomments(:one)
  end

  test "should get index" do
    get votescomments_url
    assert_response :success
  end

  test "should get new" do
    get new_votescomment_url
    assert_response :success
  end

  test "should create votescomment" do
    assert_difference('Votescomment.count') do
      post votescomments_url, params: { votescomment: { comment_id: @votescomment.comment_id, user_id: @votescomment.user_id } }
    end

    assert_redirected_to votescomment_url(Votescomment.last)
  end

  test "should show votescomment" do
    get votescomment_url(@votescomment)
    assert_response :success
  end

  test "should get edit" do
    get edit_votescomment_url(@votescomment)
    assert_response :success
  end

  test "should update votescomment" do
    patch votescomment_url(@votescomment), params: { votescomment: { comment_id: @votescomment.comment_id, user_id: @votescomment.user_id } }
    assert_redirected_to votescomment_url(@votescomment)
  end

  test "should destroy votescomment" do
    assert_difference('Votescomment.count', -1) do
      delete votescomment_url(@votescomment)
    end

    assert_redirected_to votescomments_url
  end
end

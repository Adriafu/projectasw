require "application_system_test_case"

class VotescommentsTest < ApplicationSystemTestCase
  setup do
    @votescomment = votescomments(:one)
  end

  test "visiting the index" do
    visit votescomments_url
    assert_selector "h1", text: "Votescomments"
  end

  test "creating a Votescomment" do
    visit votescomments_url
    click_on "New Votescomment"

    fill_in "Comment", with: @votescomment.comment_id
    fill_in "User", with: @votescomment.user_id
    click_on "Create Votescomment"

    assert_text "Votescomment was successfully created"
    click_on "Back"
  end

  test "updating a Votescomment" do
    visit votescomments_url
    click_on "Edit", match: :first

    fill_in "Comment", with: @votescomment.comment_id
    fill_in "User", with: @votescomment.user_id
    click_on "Update Votescomment"

    assert_text "Votescomment was successfully updated"
    click_on "Back"
  end

  test "destroying a Votescomment" do
    visit votescomments_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Votescomment was successfully destroyed"
  end
end

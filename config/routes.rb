Rails.application.routes.draw do
  resources :votescomments
  resources :replies
  
  resources :comments do 
    put 'vote', on: :member
    put 'unvote', on: :member
  end
  resources :contributions do
    put 'vote', on: :member
    put 'unvote', on: :member
  end
  resources :users

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root 'contributions#index'
 # post '/comments', as: :create_comment
 
  get "/logout", to: "sessions#destroy"
  get '/auth/:provider/callback', to: 'sessions#omniauth'
 
  get '/newest', to: 'contributions#newest', as: :newest
  get '/asks', to: 'contributions#ask', as: :asks
  get "/notlog", to: "contributions#showNotLogMessage"
  get '/contributions/:id', to: 'contributions#show', as: :show_contributions
  get '/contributionsUser/:idUser', to: 'contributions#contributionsUser', as: :show_submissionsUser
  get '/upvotedsubmissions', to: 'contributions#upvotedContributions', as: :show_upvotedContributions
  
  get '/comments/:id', to: 'comments#show', as: :show_comments
  get '/commentsUser/:idUser', to: 'comments#commentsUser', as: :show_commentsUser
  get '/upvotedComments', to: 'comments#upvotedComments', as: :show_upvotedComments
  
  get '/users/:id', to: 'users#show', as: :show_users
  get '/users/:id/edit', to: 'users#edit', as: :edit_users
    
end
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_04_20_132901) do

  create_table "comments", force: :cascade do |t|
    t.string "text"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "author_id"
    t.string "author"
    t.integer "contributions_id"
    t.integer "comments_id"
    t.index ["comments_id"], name: "index_comments_on_comments_id"
    t.index ["contributions_id"], name: "index_comments_on_contributions_id"
  end

  create_table "contributions", force: :cascade do |t|
    t.string "title"
    t.string "url"
    t.integer "points", default: 1
    t.string "user"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "usersVoted", default: "-1,"
    t.string "text", default: ""
    t.integer "userID"
  end

  create_table "replies", force: :cascade do |t|
    t.integer "comm_id"
    t.integer "repl_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "text", default: ""
    t.integer "root_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "uid"
    t.string "token"
    t.string "provider"
    t.integer "karma", default: 0
    t.string "about"
    t.string "showdead", default: "no"
    t.string "noprocrast", default: "no"
    t.integer "maxvisit", default: 0
    t.integer "minaway", default: 0
    t.integer "delay", default: 0
  end

  create_table "votescomments", force: :cascade do |t|
    t.integer "comment_id"
    t.integer "user_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "comments", "comments", column: "comments_id"
  add_foreign_key "comments", "contributions", column: "contributions_id"
end

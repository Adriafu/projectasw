class ContributionsAddicionalAttributes < ActiveRecord::Migration[6.1]
  def change
    add_column :contributions, :usersVoted, :string, default: "-1,"
    add_column :contributions, :text, :string, default: ""
    add_column :contributions, :userID, :integer
    change_column :contributions, :points, :integer, default: 1
    
  end
end

class CommentsAddicionalAtributes < ActiveRecord::Migration[6.1]
  def change
    add_column :comments, :author_id, :integer
    add_column :comments, :author, :string
    add_reference :comments, :contributions, foreign_key: true
    add_reference :comments, :comments, foreign_key: true
  end
end

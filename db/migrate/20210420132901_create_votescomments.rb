class CreateVotescomments < ActiveRecord::Migration[6.1]
  def change
    create_table :votescomments do |t|
      t.integer :comment_id
      t.integer :user_id

      t.timestamps
    end
  end
end

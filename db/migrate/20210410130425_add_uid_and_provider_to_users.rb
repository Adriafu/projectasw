class AddUidAndProviderToUsers < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :uid, :string
    add_column :users, :token, :string
    add_column :users, :provider, :string
    add_column :users, :karma, :integer, default: 0
    add_column :users, :about, :string
    add_column :users, :showdead, :string, default: "no"
    add_column :users, :noprocrast, :string, default: "no"
    add_column :users, :maxvisit, :integer, default: 0
    add_column :users, :minaway, :integer, default: 0
    add_column :users, :delay, :integer, default: 0
  end
end
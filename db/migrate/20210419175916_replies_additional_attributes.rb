class RepliesAdditionalAttributes < ActiveRecord::Migration[6.1]
  def change
    add_column :replies, :text, :string, default: ""
    add_column :replies, :root_id, :integer
  end
end

class CreateReplies < ActiveRecord::Migration[6.1]
  def change
    create_table :replies do |t|
      t.integer :comm_id
      t.integer :repl_id

      t.timestamps
    end
  end
end
